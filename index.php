<?
	//session_destroy();
	session_start(); // maybe should have a session handler latter
 	include("init.php");

	// specify parameters for autoloading classes
	spl_autoload_register(NULL, FALSE);
	spl_autoload_extensions('.php');
	spl_autoload_register('Autoloader');

	// define custom ClassNotFoundException exception class
	class ClassNotFoundException extends Exception{}

	// define Autoloader class
	function Autoloader($classname)
	{
			$classpath = explode('_', $classname);
			$class = array_pop($classpath);

			if (class_exists( $classname, FALSE)) return;	

			//Is package, site class or default ?
			if( isset($classpath[0]) && $classpath[0] == 'Package' ){
				$file = PKG_PATH . $class . '/controllers/' . strtolower($class) . '.php';
			}
			elseif( isset($classpath[0]) && $classpath[0] == 'Site'){
				$file = APP_PATH . 'Site/controllers/'. strtolower($class) . '.php';
			}
			else{
				$file = APP_PATH . 'Core/' . strtolower($class) . '.php' ;
			}
			
			if (!file_exists($file)) throw new Exception('File ' . $file . ' not found.');
			
			require_once($file);

			if (!class_exists($classname, FALSE))
			{
				eval('class ' . $classname . '{}');

		 		throw new ClassNotFoundException('Class ' . $classname . ' not found.');
			}
	}

	// handle request and dispatch it to the appropriate controller
	try{
		Dispatcher::dispatch();
	}
	catch (ClassNotFoundException $e){
		echo $e->getMessage();
		exit();
	}

	catch (Exception $e){
		echo $e->getMessage();
		exit();
	}

	
