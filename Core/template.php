<?
	/**
	* 	Template loading class
	*/
	class Template
	{
		private $data = array();
		
		/*
			Constructor
		*/
		private function __construct($template, $data) {
			
			$this->data = !empty($data) ? $data : $this->__getCallerData();
			
			$this->__load($template);
		}

		/*
			Loads the temlpate in a factory fashion
		*/
		public static function load($template='default', $data=NULL){

			return new Template($template, $data);
			
		}

		/*
			Loads the temlpate in a factory fashion, but instead of outputing returns in a buffer string
		*/
		public static function buffer($template='default', $data=NULL){

			ob_start();

			new Template($template, $data);

			return ob_get_clean();
			
		}

		/*
			Fetches the data from the Controller that called Template::load()
		*/
		private function __getCallerData(){
			$trace = debug_backtrace();


			//if Template has been called by another class and that class is a controller...
			//trace 3 is caller object, 2 is load, 1 is __construct, 0 is this!
			if (isset($trace[3]) && is_subclass_of( $trace[3]['object'], 'Controller' )) {
				
				//gets the data to the template, by calling getData which all controllers have.
				return $trace[3]['object']->getData();
			}
			return array();
		}


		/*
			Loads the temlpate file
		*/
		private function __load($template){
			include(APP_PATH."Templates/$template.php");
		}

		/*
			Set a data value
		*/
		private function set($name, $value){
			$this->data[$name] = $value;
		}

		/*
			Retrieves a data value, if exists, or '' if not
		*/
		private function get($name){
			return isset($this->data[$name]) ? $this->data[$name] : '';
		}


	}
?>