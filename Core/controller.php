<?
	/**
	* Controller class
	*/
	class Controller
	{
		protected $Model = null;
		protected $data = null;

		function __construct()
		{
			$this->Model = $this->getModel();
		}

		public function getModel($model=null){

			$package = str_replace('Package_', '', get_class($this) );

			$classname = $package . '_Model';

			$package = str_replace('Site_', '', $package );

			$r = new ReflectionClass($this);

			$path = dirname( $r->getFileName() ) . '/../models/' ;

			$file = $path . strtolower($package) . 'Model.php';
			
			if( file_exists($file) )
				return Helper::getClass($classname, $file);
		}

		public function showView($view='default'){

			//$package = str_replace('Package_', '', get_class($this) );
			//$file = PKG_PATH . $package . '/views/' . $view . '.php';

			$r = new ReflectionClass($this);

			$path = dirname( $r->getFileName() ) . '/../views/' ;

			$file = $path . $view . '.php';

			include $file;

		}

		public function getView($view='default'){

			ob_start();

			$this->showView($view);

			return ob_get_clean();
		}

		/*
			Set a data value
		*/
		public function set($name, $value){
			$this->data[$name] = $value;
		}

		/*
			Retrieves a data value, if exists, or '' if not
		*/
		public function get($name){
			return isset($this->data[$name]) ? $this->data[$name] : '';
		}

		/*
			Retrieves all the data
		*/
		public function getData(){
			return $this->data;
		}

		// /*
		// 	Replaces all the data
		// */
		// public function setData($data){
		// 	$this->data = $data;
		// }
	}