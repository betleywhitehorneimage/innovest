<?
/**
* Database handling class, a realy simlpe one :)
*/
class DB
{    
    private function __construct(){}

    private function db_connect(){
        try {
            //tries to ceate a db handler and returns it
            return new PDO(DB_DSN, DB_USER, DB_PASS);

        }
        catch (PDOException $error) {
            die('Database Connection failed: '.$error->getMessage());
        }
    }

    public static function query($sql, $params = array(), $lastid=false){
        $dbh = self::db_connect();
        $st = $dbh->prepare($sql);
        $r = $st->execute($params);
        
        return $lastid ? $dbh->lastInsertId() : $r;// returns last id or statement;
    }

    public static function get_all($sql, $params = array()){
        $dbh = self::db_connect();
        $st = $dbh->prepare($sql);
        $st->execute($params);
        
        return $st->fetchAll(PDO::FETCH_ASSOC);
    }

    public static function get_row($sql, $params = array(),$rownum=0){
        $dbh = self::db_connect();
        $st = $dbh->prepare($sql);
        $st->execute($params);
        
        return $st->fetch(PDO::FETCH_ASSOC,$rownum);
    }

    public static function get_one($sql, $params = array(),$colnum=0){
        $dbh = self::db_connect();
        $st = $dbh->prepare($sql);
        $st->execute($params);
        return $st->fetch(PDO::FETCH_COLUMN,$colnum);
    }

    public static function get_meta($table, $id, $lang='en', $field = null){

        $field = empty($field) ? '' : "AND field = '$field'";

        $metafields = self::get_one("SELECT
                            GROUP_CONCAT(DISTINCT 
                                CONCAT( 'MAX(IF(field = \"', `field`, '\",value,NULL)) AS ', `field`)
                            )
                            FROM meta WHERE kin = '$table' AND pid = $id AND lang='$lang' $field");;

         $sql = "SELECT $metafields
                FROM meta
                WHERE `kin`= \"$table\" AND `pid` = $id AND `lang` = '$lang' $field";
        return self::get_row($sql);
    }

    public static function set_meta($table, $id, $lang='en', $fields = null){

        foreach ($fields as $field => $value) {

            self::query("INSERT into meta (`kin`,`pid`,`lang`,`field`,`value`) VALUES(:table,:id,:lang,:field,:value)
             ON DUPLICATE KEY UPDATE `value` = :value",
             array(':table'=>$table,':id'=>$id,':lang'=>$lang,':field'=>$field, ':value'=>$value));

        }
    }
}