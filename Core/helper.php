<?
	/**
	* 
	*/
	class Helper
	{

		public static function getClass($classname, $file){
			
			if (!file_exists($file)) throw new Exception('File ' . $file . ' not found.');

			require_once($file);

			if (!class_exists($classname, FALSE))
			{
				eval('class ' . $classname . '{}');

		 		throw new ClassNotFoundException('Class ' . $classname . ' not found.');
			}

			return new $classname();

		}

		public static function get_excerpt($text,$max=25,$cutChars=false){

			$result = strip_tags($text); 

			if($cutChars){
				if (strlen($result)>$max)
				{
					$result=substr($result, 0, $max);
					$pos = strrpos($result, " ");
					if ($pos>0) { $result = substr($result, 0, $pos)." ..."; }
				}
			}
			else{
				$array = explode(" ", $result);

				if (!count($array)<=$max)
				{
					array_splice($array, $max);
					$result = implode(" ", $array)." ...";
				}
			}

			return $result;
		}

		//A class exists wrapper that returns only true of false, discarding the exceptions.
		public static function my_class_exists($class){

			try {
				return class_exists($class);
			} catch (Exception $e) {
				return false;
			}

		}

		public static function slug_it($str){		
			//replaces all invalid characters and removes duplicated dashes
			$str = strtolower(trim($str));
			$str = preg_replace('/[^a-z0-9-]/', '-', $str);
			$str = preg_replace('/-+/', "-", $str);
			
			return $str;
		}

		function array_get($array, $keys, $prefix=''){
	    	$keys = str_replace(' ', '', $keys);
	    	$keys = explode(',', $keys);
	    	foreach ($keys as $i => $key) {
	    		if ($prefix!==false) {
	    			$new_arr[$prefix.$key] = $array[$key];
	    		}
	    		else{
	    			$new_arr[$i] = $array[$key];
	    		}
	    	}
	    	return $new_arr;
	    }

	}