<?
	/**
	* 
	*/
	class DEBUG
	{
		
		public static function get($args=null)
		{
			$args = func_get_args();

			$fp = fopen(APP_PATH . 'debug.txt', 'a');
			foreach ($args as $a) {
				ob_start();
				var_dump($a);
				fwrite($fp, ob_get_clean() . "\n" );
			}
			fclose($fp);

		}

		public static function show(){
			if (file_get_contents(APP_PATH . 'debug.txt')) {
				$output = '<div style="z-index:999; width:100%; height:250px; overflow-y: scroll; position: fixed; bottom: 0; margin: 0 auto; border: 4px solid red; background: rgba(255,255,255,0.95); color: black; font: courier;"><pre style="padding:10px;">';
				$output .= nl2br( file_get_contents(APP_PATH . 'debug.txt') );
				$output .= '</pre></div>';
				echo $output;
				file_put_contents(APP_PATH . 'debug.txt', "");
			}
		}
	}