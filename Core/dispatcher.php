<?
	/**
	* 
	*/
	class Dispatcher
	{
		
		private function __construct()
		{
		}

		public static function dispatch(){
	
			//takes the site root and eventual trailing slash to an array with all the segments
			$segments = explode('/', trim( preg_replace('|'.SITE_ROOT.'|', '', $_SERVER['REQUEST_URI'],1),'/') );

			//array_shift($segments); //take first bit;

			//test if s0 is site controller
			if( isset($segments[0]) && Helper::my_class_exists('Site_'.$segments[0]) ){
				$controller = 'Site_' . $segments[0];

				if( isset($segments[1]) && is_callable( array($controller, str_replace('-', '_', $segments[1]) ) ) ){
					
					$method = str_replace('-', '_', $segments[1] );
					
					$arg = !empty($segments[2]) ? array_slice($segments, 2) : NULL;
				}else{
					
					$method = 'index';
					
					$arg = !empty($segments[1]) ? array_slice($segments, 1) : NULL;	
				}

			}
			//test if s0 is default site controller method
			elseif( isset($segments[0]) && is_callable( array('Site_Default', str_replace('-', '_', $segments[0]) ) ) ){

				$controller = 'Site_Default';

				$method = str_replace('-', '_', $segments[0]); //in case we want dashes on the urls method name

				$arg = !empty($segments[1]) ? array_slice($segments, 1) : NULL;	
			}
			else{
				$controller = 'Site_Default';

				$method = 'index'; // >> this one calls pages, etc

				$arg = !empty($segments[0]) ? $segments : NULL;
			}

			// attempt to instantiate the proper controller and call the requested method via the Request class
			Request::load($controller, $method, $arg);

		}
	}