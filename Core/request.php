<?php

	class Request
	{
		private static $_controllerInstance = array();

		// private constructor
		private function __construct(){}

		// instantiate proper controller with the requested method
		public static function load($controller, $method, $arg = NULL)
		{

			// check if the requested controller has been already instantiated
			if (!isset(self::$_controllerInstance[$controller]))
			{
				// if not, try to instantiate the controller
				if (!class_exists($controller)) throw new Exception('Call to invalid controller class.');

				self::$_controllerInstance[$controller] = new $controller;

				// check if controller method can be called
				if (!is_callable(array(self::$_controllerInstance[$controller], $method))) throw new Exception('Call to invalid controller method.');
			}

			// call method in requested controller and return output when applicable

			$args = array_slice(func_get_args(), 2) ; //get the args passed, which may vary in number

			$reflectionMethod = new ReflectionMethod($controller, $method);
		
			return $reflectionMethod->invokeArgs(self::$_controllerInstance[$controller], $args); // calls the method with args


		}
	}