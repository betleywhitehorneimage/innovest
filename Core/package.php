<?
	/**
	* 
	*/
	class Package
	{
		
		private function __construct(){}

		public static function load($package,$controller=null)
		{			
			if( !$controller ){
				$classname = 'Package_' . $package;
				$file = PKG_PATH . $package . '/controllers/' . strtolower($package) . '.php';
			}
			else{
				$classname = 'Package_' . $package . '_' . $controller;
				$file = PKG_PATH . $package . '/controllers/' . strtolower($controller) . '.php';
			}

			return self::getClass($classname, $file);
		}

		public static function model($package){
			
			$classname = $package . '_Model';
			
			$file = PKG_PATH . $package . '/models/' . strtolower($package) . 'Model.php';
			
			return self::getClass($classname, $file);
		}


		private function getClass($classname, $file){
			
			if (!file_exists($file)) throw new Exception('File ' . $file . ' not found.');

			require_once($file);

			if (!class_exists($classname, FALSE))
			{
				eval('class ' . $classname . '{}');

		 		throw new ClassNotFoundException('Class ' . $classname . ' not found.');
			}

			return new $classname();

		}

	}