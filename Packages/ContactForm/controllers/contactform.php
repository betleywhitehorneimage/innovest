<?
	/**
	* 
	*/
	class Package_ContactForm extends Controller
	{
		
		public function contactUs(){

			$this->set("valid",false);

			if ($_SERVER["REQUEST_METHOD"] == "POST" && isset($_POST["email"]) ){
								 
				$this->sendmail($_POST);
				$this->set("valid",true);
				
			}

      if ($_SESSION["lang"] == 2) return $this->getView("contactDE");
			return $this->getView();
		}

		public function sendmail($post){
			$subject="Contact request from InnoVest website";
			$to = "David.cameron@stmlife.com, Fiona.bayne@stmlife.com, Alexander.herr@stmlife.com, Iain.farr@stmgroupplc.com";

			$body =
			"Name: ".$_POST["nme"].
			"\n" .
			"Email Address: ".$_POST["email"].
			"\n" .
			"Contact Number: ".$_POST["tel"].
			"\n" .
			"Comments/Query: ".$_POST["comments"];

			if (mail($to, $subject, $body)) {
				$_SESSION["notification"]['contact-form'] = "Form Sent! Thank you for your enquiry, we will contact you shortly.";
			} else {
				$_SESSION["notification"]['contact-form'] = "Form Error! Delivery of your message has failed.";
			}
		}

	}
?>