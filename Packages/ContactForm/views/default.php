<h2>Contact Form</h2>
<link rel="stylesheet" href="<?=ASSETS?>css/contactform.css">

<? if ( isset($_SESSION["notification"]['contact-form']) ): ?>
	<div class="notification">
		<?=$_SESSION["notification"]['contact-form']?> <a href="#" class="close-message">[ &#10006; close]</a>
	</div>
	<br>
	<? unset($_SESSION["notification"]['contact-form']); ?>
<? endif;?>
<form id="contact-form" action="" method="post">
	<table width="100%" border="0" cellpadding="3" cellspacing="3">
	
		<div class="input-wrapper">
			<label>Your Name</label>
			<input name="nme" type="text" class="required" value="<?=$this->get('valid') ? '' : @$_POST['nme']?>">
		</div>
		<div class="input-wrapper">
			<label>Email Address</label>
			<input name="email" type="text" class="required email" value="<?=$this->get('valid') ? '' : @$_POST['email']?>">
		</div>
		<div class="input-wrapper">
			<label>Contact Number</label>
			<input name="tel" type="text" value="<?=$this->get('valid') ? '' : @$_POST['tel']?>">
		</div>
		<div class="input-wrapper">
			<label>Comment/Query</label>
			<textarea name="comments" class="required"><?=$this->get('valid') ? '' : @$_POST['comments']?></textarea>
		</div>
		<div class="command-wrapper">
			<input type="submit" value="Send Message" class="btnok">
		</div>
	</table>
</form>

<script type="text/javascript" src="<?=ASSETS?>js/jquery.validity.js"></script>
<script type="text/javascript">
	$("#contact-form").validity();

	$(".close-message").on("click", function(e){
		e.preventDefault();
		$(this).closest(".notification").fadeOut();
	});

</script>