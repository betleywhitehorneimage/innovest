<?php if ( $this->get('menu') ):  ?>
	<ul id="menu-list-<?=$this->get('menuname')?>" class="menu-list">
		<? foreach ( $this->get('menu') as $k => $item) : ?>
			<li id="menuitems-<?=$item['id']?>" class="<?= $k == (count($this->get('menu')) - 1) ? 'last' : ''; ?>" >
				<a class="<?= ($item["id"]==@$this->get('currentid') || $item['id']==@$this->get('currentsubof')) ? ' active' : ''?>" href="<?=SITE_ROOT . 'admin/' . $item["id"] . '/' . $item['fullpath'] ?>"><?= $this->get('caps') ? strtoupper($item["menuname"]) : $item["menuname"]; ?></a>
			</li>
		<? endforeach;?>
	</ul>

	<script src="<?= ASSETS ?>js/jquery-ui.min.js" type="text/javascript" charset="utf-8"></script>
	<script type="text/javascript">
		$('.menu-list').sortable({
			opacity: 0.4,
			update: function( event, ui ) {
				var id = $(this).attr('id').split('-')[2];
				$.get("<?=SITE_ROOT?>request/reorderMenu/", $(this).sortable("serialize") + "&menu="+id);
				$(this).find('li').removeClass('last').last().addClass('last');
			}
		}).disableSelection();
	</script>
<?php endif ?>