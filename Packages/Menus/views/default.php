<?php if ( $this->get('menu') ): ?>
	<ul id="menu-list-<?$this->get('menuname')?>" class="menu-list">
		<? foreach ( $this->get('menu') as $k => $item) : ?>
			<li class="<?= $k == (count($this->get('menu')) - 1) ? 'last' : ''; ?>" >
				<a class="<?= ($item["id"]==@$this->get('currentid') || $item['id']==@$this->get('currentsubof')) ? ' active' : ''?>" href="<?=SITE_ROOT . $item["fullpath"] ?>"><?= $this->get('caps') ? strtoupper($item["menuname"]) : $item["menuname"]; ?></a>
			</li>
		<? endforeach;?>
	</ul>
<?php endif ?>