<?
	/**
	* 
	*/
	class Package_Menus extends Controller
	{

		public function topMenu($mode='default'){

			$currentPage = Request::load('Package_Pages','getPage');

			$this->set("currentid", $currentPage->get('id') );
			$this->set("currentsubof", $currentPage->get('subof') );

			$this->set('caps',true);
			$this->set("menuname", 1);

			$this->set("menu", $this->Model->getTopMenu($mode) );
			return $this->getView($mode);

		}

		public function bottomMenu($mode='default'){
			$this->set("menuname", 2);
			$this->set("menu", $this->Model->getBottomMenu() );			

			return $this->getView($mode);
		}

		public function subMenu($id,$mode='default'){
			$currentPage = Request::load('Package_Pages','getPage');
			
			$this->set("currentid", $currentPage->get('id') );
			$this->set("currentsubof", $currentPage->get('subof') );

			$this->set('caps',true);
			$this->set("menuname", "sub$id");
			$this->set("menu", $this->Model->getSubMenu($id) );			

			return $this->getView($mode);
		}

	}
?>