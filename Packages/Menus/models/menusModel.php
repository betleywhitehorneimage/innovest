<?

	/**
	* 
	*/
	class Menus_Model
	{		
		function __construct()
		{
		}

		private static function getMenu($parent, $menu, $aktiveonly=1){
			$menuToSearch = ($parent != 0) ? "sub$parent" : $menu;
			$order = DB::get_one("SELECT value from config WHERE varname=? UNION SELECT 0 LIMIT 0,1",array('menu-'.trim( $menuToSearch ).'-order'));
			$pages = DB::get_all("SELECT id,menuname,get_fullpath_lang(id) as fullpath FROM pages WHERE subof=$parent AND locate($menu,inmenu) AND language = " . Menus_Model::get_language() . " AND aktiv in($aktiveonly) ORDER BY FIELD (id,$order)");
			return $pages;
		}

		public static function getTopMenu($mode){
		
			if($mode=='admin'){
				return array_merge( self::getMenu(0,'0','0,1') , self::getMenu(0,'1','0,1') );
			}
			else{
				return self::getMenu(0,1);
			}
		}

		public static function getBottomMenu(){
			return self::getMenu(0,2);
		}

		public static function getSubMenu($id){
			return self::getMenu($id,1);
		}

    private static function get_language(){
    	return !empty($_SESSION["lang"]) && is_numeric($_SESSION["lang"]) ? $_SESSION["lang"] : 1;
    }
	}