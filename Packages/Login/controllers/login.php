<?
	/**
	* 
	*/
	class Package_Login extends Controller
	{
		
		function __construct()
		{
		}

		public function login(){
			unset($_SESSION['resource']);
			$strRedirect = "/partner-login";

			if( $_SERVER['REQUEST_METHOD'] == 'POST' && $this->getModel()->loggin(trim($_POST['username']),trim($_POST['password'])) ){
				if ($_POST['username'] == "resource") {
					$_SESSION['resource'] = true;
					header("Location: .$strRedirect");    
				} else {
	  			$_SESSION['loggedin'] = true;
  				$_SESSION['loggeduser'] = $_POST['username'];
					if ($_SERVER["REDIRECT_URL"] == $strRedirect) header("Location: .$strRedirect");
  			}
			}
			else{
        if ($_SERVER["REDIRECT_URL"] == $strRedirect) return $this->getView('login');
				$this->set('content', $this->getView('login') );

				Template::load('default');

				exit();
			}
		}

		public static function isLoggedIn(){
			return isset($_SESSION['loggedin']);
		}


		public static function logout(){
			unset($_SESSION['loggedin']);
			unset($_SESSION['resource']);
			header("Location: " . SITE_ROOT);
		}


	}
	