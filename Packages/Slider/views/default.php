

<link rel="stylesheet" href="<?=ASSETS?>css/slider.css">
<? $slides = $this->get('slides'); ?>

<div id="slider">
	<?php foreach ($slides as $k => $slide): ?>
		<div id="slide-<?=$k?>" class="slide <?=$k==0 ? 'active' : ''?>" style="z-index:<?=$k==0? 3 : 1 ?>; background-image: url(<?= $slide->img?>)">
			<span class="slide-caption"><?=$slide->caption?></span>
			<div class="slide-overlay">
				<?=$slide->text ?>
				<div class="slide-navigation">
					<? for($i=0; $i<count($slides); $i++){ ?>
						<a id="slide-nav-<?=$i?>" href="#slide-<?=$i?>" class="slide-nav <?= $i == $k ? 'active' : '' ?>">&nbsp;</a>
					<? } ?>
				</div>
			</div>
		</div>	
	<?php endforeach ?>
</div>

<script type="text/javascript">
	var slideplay = true;
	$(document).ready(function(){

		setInterval(function() {
			if (slideplay){
				var active = $(".slide.active");

				if( ! active.next().hasClass('slide') ){
					var next = $(".slide:first");
				}
				else{
					var next = active.next();
				}
				console.log(next.id);

				next.css('z-index',2);

				active.fadeOut(750,function(){
				  active.css('z-index',1).show().removeClass('active');
				  next.css('z-index',3).addClass('active');
				});
			}
		}, 3500);
	});

	$("#slider").on('mouseenter',function(){ slideplay = false });
	$("#slider").on('mouseleave',function(){ slideplay = true });

	$(document).on('click','.slide-nav',function(){
		var id = $(this).attr('id').split('-')[2];
 		var active = $(".slide.active");
		if ( active.attr('id').split('-')[1] != id ) {
	 		var active = $(".slide.active");
	 		var next = $("#slide-"+id);
	 		next.css('z-index',2);		
			active.fadeOut(350,function(){
				active.css('z-index',1).show().removeClass('active');
				next.css('z-index',3).addClass('active');
			});

		};
		return false;
	});

</script>

<!--[if lt IE 9]>
<script>
	slideplay = false;
</script>
<![endif]-->