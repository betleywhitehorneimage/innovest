<?
	/**
	* 
	*/
	class Slider_Model
	{

		public static function getSlides(){

			//for the time being, we will store this in a json file. if needed in the future this can be converted to db.
			$file =dirname( __FILE__ ) . (isset($_SESSION["lang"]) && $_SESSION["lang"] == 2 ? "/slidesDE.json" : "/slides.json");
			$slides = json_decode(file_get_contents($file));

			return $slides;
		}		
	}