<?

	class Package_Resource extends Controller
	{
		
		function __construct()
		{
		}
		
		public function Resource(){
			if (!empty($_SESSION['resource'])) return $this->show();
			else if (!empty($_SESSION['loggedin'])) return $this->admin();
			else return Package::load('Login')->login();
        }
        
        private function admin(){
        	$strError = "";
        	if (!empty($_FILES["file"]["name"])){
        		if ( empty($_FILES["file"]["type"]) || !( $_FILES["file"]["type"] == "application/pdf" || $_FILES["file"]["type"] == "application/vnd.ms-excel" || $_FILES["file"]["type"] == "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" ) ){
        			$strError = "This file is not supported.";
        		} elseif (!empty($_FILES["file"]["error"])){
        			$strError = "An error has occurred in the upload.";
        		} else {
        	    $arrMaxID = $this->getModel()->getLatestID();
        	    $strFile = ((empty($arrMaxID[0]["intID"]) ? 0 : $arrMaxID[0]["intID"]) + 1) . "_" . str_replace(" ", "_", $_FILES["file"]["name"]);
        		  $strPath = "/Site/assets/resource/$strFile";
        		  if (file_exists($strPath)) unlink($strPath);
        		  if (move_uploaded_file($_FILES["file"]["tmp_name"], $_SERVER["DOCUMENT_ROOT"] . $strPath)){
        		  	chmod($_SERVER["DOCUMENT_ROOT"] . $strPath, 0766);
        		  	$this->getModel()->newFile($_FILES["file"]["name"], $strPath);
        		  }
        	  }
        	} elseif (!empty($_POST["submit"]) && $_POST["submit"] == "Update") {
        		$this->getModel()->update($_POST["intID"],$_POST["dname"], $_POST["description"], $_POST["category"]);
        	} elseif (!empty($_POST["submit"]) && $_POST["submit"] == "Delete") {
        		$arrResult = $this->getModel()->getFiles($_POST["intID"]);
        		if (unlink($_SERVER["DOCUMENT_ROOT"] . $arrResult[0]["path"])) $this->getModel()->deleteFile($_POST["intID"]);
        	}
        	
        	$this->set('strError', $strError);
        	$this->set('arrFiles', $this->getModel()->getFiles());
            $this->set('categories', $this->getModel()->getCategories());
            return $this->getView('admin');
        }
        
        private function show(){    	
          $this->set('files', $this->getModel()->getFiles());
          $this->set('categories', $this->getModel()->getCategories());
          return $this->getView('default');
        }

	};
  
?>