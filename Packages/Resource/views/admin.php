<h1>Upload file</h1>

<a href="/admin/logout" class="resourceLogout">Logout?</a>
<form action="" method="POST" enctype="multipart/form-data">
  <label>Add new file:</label>
  <input type="file" name="file" id="file" onChange="submit();">
  <input type="hidden" name="action" value="add">
</form><br />
<?
  $strError = $this->get('strError');
  $arrFiles = $this->get('arrFiles');
  $strPath = "/Site/assets/resource/";
  $strPathIcon = "/Site/assets/images/pdf.jpg";
  
  if (!empty($strError)): ?>
    <p class="resourceError"><?=$strError?></p>
  <?php endif;
  
  $categories = $this->get('categories');

   /*$categories = array(
      0 => 'Not on display',
      1 => 'InnoVest Life',
      2 => 'InnoVest Cap',
      3 => 'InnoVest Trust',
      4 => 'Berkeley Hannover',
      5 => 'Intermediary Agreement',
      6 => 'InnoVest',
    );*/

  for ($i=0; $i < count($arrFiles); $i++){
  ?>
    <form action="" class="resourceItem" method="POST">
      <label>Display Name:</label><input type="text" name="dname" value="<?=$arrFiles[$i]["name"]?>"><a href="<?=$arrFiles[$i]["path"]?>" target="_blank" class="download">Download</a><br>
      <label>Area</label><select name="category">
        <option value="0">Not on display</option>
        <?php foreach ($categories as $category): ?>
          <option value="<?=$category['id']?>" <?= $arrFiles[$i]["category"] == $category['id'] ? 'selected="selected"' : '' ?>><?=empty($category['parent']) ? '' : '- ' ?><?=$category['name']?></option>
        <?php endforeach ?>
      </select><br>
      <label>Description</label><textarea name="description" id="" rows="5"><?=$arrFiles[$i]["description"]?></textarea><br>
      <input type="hidden" name="action" value="amend">
      <input type="hidden" name="intID" value="<?=$arrFiles[$i]["id"]?>">
      <label>&nbsp;</label><input type="submit" name="submit" value="Update">&nbsp;<input type="submit" name="submit" value="Delete">
    </form>
  <?php
  }
