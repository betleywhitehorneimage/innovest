<a href="/admin/logout" class="resourceLogout">Logout?</a>
<h1>Resources</h1>
<div id="resource-list">
<?
  $filter_callback = function($cat){ return function($file) use ($cat){ return $file['category'] == $cat; }; }; 
  $cats_callback = function($parent){ return function($cats) use ($parent){ return $cats['parent'] == $parent; }; }; 
  $files = $this->get('files');
  $cats = $this->get('categories');
  $intTemp = 0;

  $parents = array_filter($cats,$cats_callback(0));

  foreach ($parents as $cat){
    $cat_files = array_filter($files,$filter_callback($cat['id'])); 
    if (!empty($cat_files) ) : ?>
    <div class="resource-category">
        <h4><?=$cat['name']?></h4>
        <?php foreach ( $cat_files as $file): ?>
          <? $type = pathinfo($file['path'], PATHINFO_EXTENSION); ?>
          <p><i class="<?= strtolower($type) == 'xls' || strtolower($type) == 'xlsx' ? 'excel' : '' ?>"></i><a href="<?=$file['path']?>" target="_blank" title="<?= $file['description'] ?>"><?= $file['name'] ?></a></p>        
        <?php endforeach ?>

        <?php if (!empty($cat['children'])): ?>
          <?php $children = array_filter($cats,$cats_callback($cat['id'])); ?>
          <?php foreach ($children as $child):
            $cat_files = array_filter($files,$filter_callback($child['id']));

            if (!empty($cat_files) ) : ?>
              <div class="resource-subcategory">
                <h5><?=$child['name']?></h5>
                <?php foreach ( $cat_files as $file): ?>
                  <? $type = pathinfo($file['path'], PATHINFO_EXTENSION); ?>
                  <p><i class="<?= strtolower($type) == 'xls' || strtolower($type) == 'xlsx' ? 'excel' : '' ?>"></i><a href="<?=$file['path']?>" target="_blank" title="<?= $file['description'] ?>"><?= $file['name'] ?></a></p>        
                  <?php endforeach ?>
              </div>
            <?php endif ?>

          <?php endforeach ?>
          
        <?php endif ?>
          
      </div>
    <?php endif;
  }
  ?>
</div>
<div class="clear"></div>
<script src="<?=ASSETS?>js/masonry.min.js"></script>
<script>
  $('#resource-list').masonry();
</script>
