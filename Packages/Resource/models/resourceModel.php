<?
	class Resource_Model
	{
		
		function __construct()
		{
		}

		public static function newFile($strName, $strPath){			
			return DB::query("INSERT INTO documents (name, dt_added, path) VALUES (?, NOW(), ?)",
			                 array($strName, $strPath));
		}

		public static function update($intID, $strName, $strDescription, $intCategory){ 
			return DB::query("UPDATE documents SET name=?, description=?, category=? WHERE id = ?",
			                 array($strName, $strDescription, $intCategory, $intID));
		}
		
		public static function getFiles($intID = 0){
			if (empty($intID)){
				return DB::get_all("SELECT id, name, description, dt_added, path, category FROM documents ORDER BY category ASC, name ASC");
			} else {
				return DB::get_all("SELECT id, name, description, dt_added, path, category FROM documents WHERE id = ?", array($intID));
			}
		}

		public static function getCategories(){
			return DB::get_all("SELECT
								    m.id, 
								    m.name, 
								    m.parent, 
								    (SELECT GROUP_CONCAT(id) FROM document_categories WHERE parent = m.id) AS children,
								    m.ord, 
								    CASE 
								        WHEN p.ord IS NULL THEN m.ord * 100
								        ELSE m.ord + p.ord * 100
								    END AS parentId
								FROM
								    document_categories m
								    LEFT JOIN document_categories p
								    ON m.parent = p.id
								ORDER BY parentId;");
		}
		
		public static function deleteFile($intID){
			return DB::query("DELETE FROM documents WHERE id=?", array($intID));
		}
		
		public static function getLatestID(){
			return DB::get_all("SELECT MAX(id) AS intID FROM documents"); 
		}
	}

?>