<?
	/**
	* 
	*/
	class Package_Pages extends Controller
	{
		
		public function show($arg){
			/* Get the data from model*/
			$this->data = $this->Model->getPageBySlug($arg);

			if( $this->data['id'] == $this->Model->getDefaultPageId()){
				self::showDefault();
				return;
			} 

			if( !$this->data['txt'] && $this->data['children'] ) //jump to main page :: to review
			{
				$this->data = $this->Model->getPageByID( $this->Model->getChild($this->get('id')) );
			}

			/* Set the template */
			$this->set('title', $this->get('menuname') );
			$this->set('topmenu', Package::load('Menus')->topMenu() );
			
			if ( $this->get('subof') == 0 )
				$this->set('submenu', Package::load('Menus')->subMenu($this->get('id')) );
			else
				$this->set('submenu', Package::load('Menus')->subMenu($this->get('subof')) );

			switch ($this->get('slug')) {
				case 'contact-us':
				case 'kontakt':
					$this->set('content', $this->getView() . Package::load('ContactForm')->contactUs() ); 
					break;
				case 'partner-login':
					$this->set('content', $this->getView() . Package::load('Resource')->Resource()); 
					break;
				default:
					$this->set('content', $this->getView());
			}
			
			$this->set('bottommenu', Package::load('Menus')->bottomMenu() );		

			Template::load('default');

		}

		public function showDefault(){
			
			/* Get the data from model*/
			$this->data = $this->Model->getDefaultPage();

			/* Set the template */
			$this->set('title', $this->get('menuname') );
			$this->set('topmenu', Package::load('Menus')->topMenu() );
			$this->set('content', $this->getView('home') );
			$this->set('bottommenu', Package::load('Menus')->bottomMenu() );		

			Template::load('default');
		}
		

		public function search($term){
			$this->set('term',$term);
			$this->set("results", $this->Model->getPagesWith($term) );

			/* Set the template */
			$this->set('title', "Searching for $term" );
			$this->set('topmenu', Package::load('Menus')->topMenu() );
			$this->set('content', $this->getView('search') );
			$this->set('bottommenu', Package::load('Menus')->bottomMenu() );

			Template::load('default');
		}

		public function getPage(){
			return $this;
		}

		/* Admin Area */
		public function editor($id=null){

			if ( empty($id) || is_numeric($id)) {
        if ($id == 58) header("Location: http://" . $_SERVER['HTTP_HOST'] . "/partner-login");    
				/* Get the data from model*/				
				$this->data = empty($id) ? $this->Model->getDefaultPage() : $this->Model->getPageByID($id);

				if( $this->get('children') > 0 ) $this->set('subpages', $this->get('id') );

				if ( $this->get('subof') == 0 )
					$this->set('submenu', Package::load('Menus')->subMenu($this->get('id'),'admin') );
				else
					$this->set('submenu', Package::load('Menus')->subMenu($this->get('subof'),'admin') );

				$this->set('action', 'update' );
				$this->set('title', 'Editing ' . $this->get('menuname') );

			}
			else{
				$this->set('title', 'New Page' );
				$this->set('id', 'new' );
				$this->set('action', 'insert' );
				$this->set('aktiv', '1' );
				$this->set('subof', @$_GET['subof'] );
				$this->set('inmenu', @$_GET['inmenu'] );
			}
			$this->set('pagelist', $this->Model->getPageList());

			/* Set the template */
			$this->set('topmenu', Package::load('Menus')->topMenu('admin') );		
			$this->set('bottommenu', Package::load('Menus')->bottomMenu('admin') );		

			$this->set('content', $this->getView('admin') );

			Template::load('default');
		}

		/* Actions : */
		public function update($id, $post=null){
			if (isset($post)) {
				$this->Model->updatePage($id,$post);
			}

			header("Location: " . SITE_ROOT . "admin/page/" . $id);
		}

		public function delete($id){
			
			if (isset($id)) {
				$this->Model->deletePage($id);
			}

			header("Location: " . SITE_ROOT . "admin");
		}

		public function insert($post){

			if (isset($post)) {
				$id = $this->Model->insertPage($post);
			}

			header("Location: " . SITE_ROOT . "admin/page/" . $id);
		}
		
	}