<?
	/**
	* 
	*/
	class Pages_Model
	{
		
		function __construct()
		{
		}

		public static function getPageByID($id){
	   $p = DB::get_row("SELECT (SELECT COUNT(id) FROM pages WHERE subof = :id AND aktiv = 1) AS children , p.*, c.value as defpage FROM pages p left join config c on p.id = c.value and varname = 'defpage' WHERE p.id = :id AND p.language = :language",array(':id'=>$id, ':language' => Pages_Model::get_language()));
			//$p = DB::get_row("SELECT (SELECT COUNT(id) FROM pages WHERE subof = :id AND aktiv = 1) AS children , p.*, c.value as defpage FROM pages p left join config c on p.id = c.value and varname = 'defpage' WHERE p.id = :id",array(':id'=>$id));
			return $p;
		}

		public static function getChild($id,$n=0){
			$child = DB::get_one("SELECT id FROM pages WHERE subof = :id AND aktiv = 1 AND txt != '' LIMIT $n,1",array(':id'=>$id));
			return $child;
		}
	
		public static function getDefaultPageID(){
			return  DB::get_one("SELECT value FROM config WHERE varname = 'defpage' AND language = " . Pages_Model::get_language());
			//return  DB::get_one("SELECT value FROM config WHERE varname = 'defpage'");
		}

		public static function getDefaultPage(){
			$default = DB::get_one("SELECT value FROM config WHERE varname = 'defpage' AND language = " . Pages_Model::get_language());
			//$default = DB::get_one("SELECT value FROM config WHERE varname = 'defpage'");
			return self::getPageByID($default);
		}

		public static function getPageBySlug($sections){
			$id = 0;
			foreach ($sections as $s ) {
				$id = DB::get_one("SELECT id from pages where subof = ? and slug = ? AND language = ?",array($id,$s,Pages_Model::get_language()));
				//$id = DB::get_one("SELECT id from pages where subof = ? and slug = ?",array($id,$s));
				if( $id === false) return self::getDefaultPage();
			}
			return self::getPageByID($id);
		}

		/* ** */
		public static function getPageList($index=0,$level=0){
			$list = array();
			
  		$page_array = DB::get_all("SELECT id, menuname, '$level' as level from pages WHERE subof=? AND language=?",array($index,Pages_Model::get_language()));	
//			$page_array = DB::get_all("SELECT id, menuname, '$level' as level from pages WHERE subof=?",array($index));	

			foreach ($page_array as $page) {
				$list[] = $page;
				$list = array_merge($list, self::getPageList($page['id'],$level+1) );
			}

			return $list;			
		}
		
		/* ** */
		public static function getSubPages($id, $all=true){

			if($all)
				return DB::get_all("SELECT id, menuname from pages WHERE subof = ?",array($id));
			else
				return DB::get_all("SELECT id, menuname from pages WHERE subof = ? WHERE aktiv = 1",array($id));
		}

		public static function getPagesWith($term){
			return DB::get_all("SELECT id, menuname, txt, get_fullpath(id) as fullpath from pages WHERE txt LIKE :term OR menuname LIKE :term",array(':term'=>"%$term%"));
		}


		public static function updatePage($id, $post){
			$post['slug'] = Helper::slug_it($post['menuname']);

			DB::query("UPDATE pages set subof=?, aktiv=?, inmenu=?,menuname=?,txt=?,keywords=?,description=?,slug=? WHERE id=?", Helper::array_get($post, 'subof,aktiv,inmenu,menuname,txt,keywords,description,slug,id',false));

			if( @ $post['defpage'] == 1 ) DB::query("UPDATE config SET value = ? WHERE varname = 'defpage'");

		}

		public static function deletePage($id){
			
			DB::query("DELETE FROM pages WHERE id=?", array($id));
		}

		public static function insertPage($post, $lang='en'){
			$post['slug'] = Helper::slug_it($post['menuname']);
			
      $newid = DB::query("INSERT INTO pages (subof,aktiv,inmenu,menuname,txt,keywords,description,slug) VALUES (?,?,?,?,?,?,?,?)", Helper::array_get($post, 'subof,aktiv,inmenu,menuname,txt,keywords,description,slug',false),true);
      if (!empty($newid)) DB::query("UPDATE pages SET language = " . Pages_Model::get_language() . " WHERE id = $newid");

			return $newid;
		}

    private static function get_language(){
    	return !empty($_SESSION["lang"]) && is_numeric($_SESSION["lang"]) ? $_SESSION["lang"] : 1;
    }
	}