<h2>Showing search results for "<?= $this->get('term') ?>"</h2>
<br>
<?php if ($this->get('results')): ?>
	<h4> <?= count($this->get('results')) ?> pages have been found:</h4>
	<br>
	<ul id="search-page-list">
		<?php foreach ($this->get('results') as $key => $page): ?>
			<li>
				<a href="<?=SITE_ROOT . $page['fullpath']?>"><h3><?=$page['menuname']?></h3></a>
				<p><?= Helper::get_excerpt($page['txt'],$max=25);?></p>
			</li>
		<?php endforeach ?>
	</ul>
<?php else: ?>
	<h4>No pages have been found. Please check the spelling or try again later.</h4>
<?php endif ?>