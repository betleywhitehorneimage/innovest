<link rel="stylesheet" href="<?=ASSETS?>css/admin.css">
<script src="<?=ASSETS?>js/ckeditor/ckeditor.js"></script>
<script src="<?=ASSETS?>js/jquery.validity.js"></script>

<form id="page-form" action="<?=SITE_ROOT?>admin/<?=$this->get('id')?>/<?=$this->get('action')?>" method="POST" enctype="multipart/form-data">

	<div id="toolbar-wrapper">
				
		<div id="page-details" <?= $this->get('action') !='insert' ? 'style="display:none";' : ''?>>

				<h3><?= $this->get('action') !='insert' ? 'Page details' : 'Add a new page'; ?></h3>

				<input id="page-id" name="id" type="hidden" value="<?=@$this->get('id')?>">
				<!-- <input type="hidden" name="subof" value="0"> -->
				
				<div class="input-wrapper">
					<label for="menuname">Name in menu</label>
					<input name="menuname" type="text" class="required" value="<?=@$this->get('menuname') ;?>"/>
				</div>
				<div class="input-wrapper">
					<label for="inmenu">Show in menu(s)</label>
					<select name="inmenu" id="inmenu-top" <?= $this->get('subof') == 0 ? '' : 'style="display:none" disabled' ?> >
						
							<option value="1"<? if (@$this->get('inmenu')=="1") {print " selected";} ?>>Top</option>
							<option value="2"<? if (@$this->get('inmenu')=="2") {print " selected";} ?>>Bottom</option>
							<option value="1,2"<? if (@$this->get('inmenu')=="1,2") {print " selected";} ?>>Both</option>
							<option value="0"<? if (@$this->get('inmenu')=="0") {print " selected";} ?>>None</option>
					</select>
					<select name="inmenu" id="inmenu-sub" <?= $this->get('subof') == 0 ? 'style="display:none" disabled' : '' ?>>
							<option value="1"<? if (@$this->get('inmenu')!="0") {print " selected";} ?>>Yes</option>
							<option value="0"<? if (@$this->get('inmenu')=="0") {print " selected";} ?>>No</option>
					</select>
				</div>
				<div class="input-wrapper">
					<label for="subof">Parent page</label>
					<select id="subof" name="subof">
						<option value="0" <?= (@$this->get('subof')==0) ? "selected" : '' ?>>Nothing</option>
						<?php foreach ($this->get('pagelist') as $page ): ?>
							<?php if ($page['id'] != $this->get('id') ): ?>
								<option value="<?=$page['id']?>" <?= (@$this->get('subof')==$page['id']) ? "selected" : '' ?> ><?=str_repeat('-', $page['level']) . ( !empty($page['menuname']) ? $page['menuname'] : '{ ' . $page['id'] . ' }' ) ?></option>
							<?php endif ?>
						<?php endforeach ?>
					</select>
				</div>				
				<?php if (@ ! $this->get('defpage') ): ?>
					<div class="input-wrapper">
						<label for="defpage">Set as default Page?</label>
						<input name="defpage" type="checkbox" value="1" />
					</div>
				<?php endif ?>
				<div class="input-wrapper">
					<label for="description">Description</label>
					<input name="description" type="text" value="<?=@$this->get('description');?>"/>
				</div>
				<div class="input-wrapper">
					<label for="keywords">Keywords</label>
					<input name="keywords" type="text" value="<?=@$this->get('keywords');?>"/>
				</div>
				<div class="input-wrapper">
					<label for="aktiv">Page is active</label>
					<input type="hidden" name="aktiv" value="0">
					<input type="checkbox" name="aktiv" id="aktiv" value="1" <?= @$this->get('aktiv') ? 'checked' : '' ;?> >
				</div>
				
		</div>

		<div id="toolbar">
			<p>
				<a class="btn-pagedets-show" title="View page details"></a>
				
				<?= (@$this->get('action')=='insert') ? 'Adding new page &#10152;' : 'Editing <strong>'.@$this->get('menuname').'</strong> &#10152;' ?>

				<?if(@$this->get('action')=='insert'):?>
					<button class="pageinsert btnok save" type="submit">Insert</button>
				<?else:?>
					<button class="pageupdate btnok save" type="submit">Update</button>
					<? if (@$this->get('defpage')=="1") : ?>
					<span class="btnok disabled" title="You cannot delete the default page.">Delete</span> 
					<? elseif ( 0 && db_get_all("SELECT id FROM pages WHERE subof=?",array(@$page))) : ?>
					<span class="btnok disabled" title="You cannot delete this page, as it has sub menus.">Delete</span> 
					<? else : ?>
					<a class="pagedelete btnok" href="<?=SITE_ROOT?>admin/<?=$this->get('id')?>/delete">Delete</a>
					<? endif; ?>
					&nbsp;|&nbsp;
					<a class="addpage btnok" href="<?=SITE_ROOT?>admin/new">Add new page</a>
				<?endif;?>


				<span class="logout">
				<!-- <a class="btnok" href="#">Edit Articles</a>

				<a class="btnok" href="#">Edit Courses</a> -->
			
				You are loged in as <strong><?=$_SESSION['loggeduser']?></strong>, <a href="<?=SITE_ROOT?>admin/logout">logout?</a></span>
			</p>

		</div>
	</div>

	<table style="width:100%">
		 
		<tr>
			<td class="main-column" valign="top">
				
				<!-- <div class="input-wrapper">
					<input name="title" type="text" placeholder="Title" value="<?=@$this->get('title');?>"/>	
				</div> -->

				<?php if (!isset($_GET['editinline'])): ?>
					<textarea class="editor transparent dotted-border" name="txt" id="editor"><?=stripslashes(@$this->get('txt'));?></textarea>
					<script>
					    // Turn off automatic editor creation first.
					    CKEDITOR.replace( 'editor' );
					</script>
				<?php else: ?>
					<div class="editor transparent dotted-border" id="editor" contenteditable="true" style="width:100%; height:100%;">
						<?=stripslashes(@$this->get('txt'));?>
					</div>
					<script>
					    // Turn off automatic editor creation first.
					    CKEDITOR.disableAutoInline = true;
					    CKEDITOR.inline( 'editor' );
					</script>
				<?php endif ?>

			</td>
			<!--td class="right-column" valign="top">
				<?php if ($this->get('action') == 'update'): ?>
					<div class="right-box">
							<h1>Subpages of <a href="<?=SITE_ROOT?>admin/<?=$this->get('id')?>"><?=$this->get('menuname')?></a></h1>
							
							<ul class="submenu">
								<?//=navigation_list_admin(@$this->get('id'),$crumbs[1],$crumbs,'1,0');?>
								<li><a class="newpagelink" href="<?=SITE_ROOT?>admin/new?subof=<?=$this->get('id')?>&menu=1">New sub-page...</a></li>
							</ul>
					</div>
					<hr>
				<?php endif ?>
			</td-->
		</tr>				
	</table>

</form>

<script type="text/javascript">

	$("#page-form").validity({
		on_invalid:function(){
			if( !$("#page-details").is(":visible") ){
				$("#page-wrapper").animate({"margin-top":$("#page-details").height()+60},"slow");
				$("#page-details").slideDown();
			}
		}
	});
	
	$(".pagedelete").on("click",function(){
		return confirm("Do you really want to delete this page?");
	});

	$('.save').on('click',function(){
		var txt = CKEDITOR.instances.editor.getData();
		$("#page-form").append($('<input name="text" type="hidden">').val(txt));
	});
	
	<?if(@$this->get('action')!='insert'):?>

		$('html,body').height( $(document).height() - 39 );
		$('#page-wrapper').css('margin-top',39);
		

		$(".btn-pagedets-show").on("click",function(e) {
			e.preventDefault();
			if( !$("#page-details").is(":visible") ){
				$("#page-wrapper").animate({"margin-top":$("#page-details").height()+60},"slow");
				$("#page-details").slideDown();
			}
			else{
				$("#page-wrapper").animate({"margin-top":39},"slow");
				$("#page-details").slideUp();
			}
		});

	<?else:?>

		$('#page-wrapper').css('margin-top',298);

	<?endif;?>

	$('#subof').on('change',function(){
		if( $(this).val() == 0 ){
			$('#inmenu-top').show().removeAttr('disabled')
			$('#inmenu-sub').hide().attr('disabled',true);
		}
		else{
			$('#inmenu-sub').show().removeAttr('disabled')
			$('#inmenu-top').hide().attr('disabled',true);
		}
	});

</script>