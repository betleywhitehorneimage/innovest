<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
	<head>
		<title> <?= SITE_NAME ?> | <?= @ $this->get('title') ?> </title>
		<meta http-equiv="content-type" content="text/html; charset=utf-8">
		<meta name="description" content="<?= @ $this->get('keywords') ?>" /> 
		<meta name="keywords" content="<?= @ $this->get('description') ?>" / >

		<link rel="stylesheet"  href="<?= ASSETS ?>css/reset.css" type="text/css" />		
		<link rel="stylesheet"  href="<?= ASSETS ?>css/styles.css" type="text/css" />	
		<link rel="stylesheet"  href="<?= ASSETS ?>css/page.css" type="text/css" />	
		<script src="<?= ASSETS ?>js/jquery.min.js" type="text/javascript" charset="utf-8"></script>

		<?= @ $this->get('includes') ?>

	</head>
	<body>

		<div id="page-wrapper" >
			<div id="header-wrapper">
				<div id="header">
					<a href="<?=SITE_ROOT?>" id="logo"></a>
					<!-- <form action="/search/" id="search-form">
						<input type="text" name="for" placeholder="Search...">
						<button type="submit"></button>
					</form> -->
				</div>
			</div>
			
			<div id="menu-wrapper">
				<?= @ $this->get('topmenu') ?>
			</div>
				
			<div id="content-wrapper">
				<?php if (  $this->get('submenu') ): ?>					
					<div id="submenu-wrapper">
						<?= @ $this->get('submenu') ?>
					</div>
					<div id="content" class="hassubmenu">
				<?php else: ?>
					<div id="content">
				<?php endif ?>
						<?= @ $this->get('content') ?>
					</div>
					
				<div class="push "></div>
			</div>

		</div>
		<div id="footer">
			<hr>
			<?= @ $this->get('bottommenu') ?>
			<p>STM LIFE Assurance PCC PLC is regulated by the Gibraltar Financial Services Comission.</p>
			<p><?=date('Y')?> All Rights Reserved. STM Life is licenced under Gibraltar's Insurance Companies legislation which incorporates the provisions of the EU's Third Life Insurance Directive.</p>
		</div>

		<?= @ $this->get('jscript') ?>
	</body>
</html>
