/**
 * @license Copyright (c) 2003-2012, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.html or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here.
	// For the complete reference:
	// http://docs.ckeditor.com/#!/api/CKEDITOR.config

	// The toolbar groups arrangement, optimized for two toolbar rows.
	config.toolbarGroups = [
		{ name: 'clipboard',   groups: [ 'clipboard', 'undo' ] },
		{ name: 'editing',     groups: [ 'find', 'selection', 'spellchecker' ] },
		{ name: 'links' },
		{ name: 'insert' },
		{ name: 'forms' },
		{ name: 'tools' },
		{ name: 'document',	   groups: [ 'mode', 'document', 'doctools' ] },
		{ name: 'others' },
		'/',
		{ name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
		{ name: 'paragraph',   groups: [ 'list', 'indent', 'align', 'blocks'] },
		{ name: 'styles' },
		{ name: 'colors' },
		{ name: 'about' }
	];

	// Remove some buttons, provided by the standard plugins, which we don't
	// need to have in the Standard(s) toolbar.
	config.removeButtons = 'Underline,Subscript,Superscript';

	config.filebrowserBrowseUrl = '/Site/assets/js/ckeditor/kcfinder/browse.php?type=files';
	config.filebrowserImageBrowseUrl = '/Site/assets/js/ckeditor/kcfinder/browse.php?type=images';
	config.filebrowserFlashBrowseUrl = '/Site/assets/js/ckeditor/kcfinder/browse.php?type=flash';
	config.filebrowserUploadUrl = '/Site/assets/js/ckeditor/kcfinder/upload.php?type=files';
	config.filebrowserImageUploadUrl = '/Site/assets/js/ckeditor/kcfinder/upload.php?type=images';
	config.filebrowserFlashUploadUrl = '/Site/assets/js/ckeditor/kcfinder/upload.php?type=flash';

	config.contentsCss = ['/Site/assets/css/reset.css','/Site/assets/css/page.css'];
	config.font_names = 'Arial/Arial, Helvetica, sans-serif;';
	//config.uiColor = '#b5b0c5';
	//config.colorButton_colors = '';
	config.extraPlugins = 'codemirror';
	config.codemirror_theme = 'ambiance';
	config.height = 400;
};
