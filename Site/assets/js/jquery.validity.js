/*
	Plugin Name: jQuery.Validity
	Author: Antonino Ferreira
	Date: May 2013
	Info: (simple) Form validation plugin :[]
*/
(function($){  
 $.fn.validity = function(options) {

 	/* default options */
	 var defaults = { 
	   err_msg_class: "error-message",
	   valid_class : "valid",
	   invalid_class: "invalid",
	   err_message: {
	   	required: '\u2716 This field is required',
	   	email: '\u2716 This should be a valid email',
		regex: '\u2716 This should match a pattern',
		match: '\u2716 These fields do not match',
		atleastone: '\u2716 Fill at least one of these fields'
	   },
	   on_invalid: null,
	   on_valid: null
	};  
	 
	 var options = $.extend(defaults, options); 

	 /* helper obejct with functions */
	 var f = {

	 	initializeField : function(){
			var input_fld = $(this);

			//goes through the err_message object which should contain one default err msg per validation type and adds message wrapper
			for(var err_class in options.err_message) {
				if ( input_fld.hasClass(err_class) ) {
					var err_msg = $('<span class="' + options.err_msg_class + ' ' + options.err_msg_class + '-' + err_class + '" style="display:none;"></span>').text(options.err_message[err_class]);
					input_fld.after( err_msg );

					input_fld.on('change',f.isValid);
				}
			}
    		
	 	},

	 	validate: function(){
	 		/* do the validation logic here : this -> form */
	 		$(this).find('input, select, textarea').each( f.isValid );

	 		// retrun true if there are no invalid fields
	 		//return ( $(this).find('.invalid').length == 0 );
	 		var formIsValid = ( $(this).find('.invalid').length == 0 );
	 		
	 		if (formIsValid){ if ( options.on_valid) options.on_valid(); }
	 		else { if ( options.on_invalid) options.on_invalid(); }

	 		return formIsValid;
	 	},

	 	evaluate: function(field,condition, clss){
			if ( condition ){
 				field.siblings( '.' + options.err_msg_class + '-' + clss ).hide();
 				return true;
 			}
 			else{
 				field.siblings( '.' + options.err_msg_class + '-' + clss ).show();
 				return false;
 			}
	 	},

	 	getType : function(field){
			if( field.is('input') ){ return field.prop('type'); }
	 		else { return field.get(0).tagName.toLowerCase(); }
	 	},

	 	isValid : function(){
	 		var valid = true; // we start by assuming it is valid
	 		var field = $(this);
	 		var type = f.getType(field);
	 		
	 		if ( field.hasClass('required') ) {
	 			switch( type ){
	 				case 'checkbox': case 'radio':
		 				valid &= f.evaluate(field, field.prop('checked'), 'required');
	 					break;
	 				default:
		 				valid &= f.evaluate(field, field.val().length > 0 && field.val() != "" , 'required');
	 			}
	 		}

	 		if ( field.hasClass('email') ) {
	 			valid &= f.evaluate(field, !field.val().length || ( field.val().length > 0 && field.val().match(/^[a-zA-Z_.0-9'\-]+@[a-zA-Z_\-]+?(?:\.[a-zA-Z]{2,3}){1,2}$/) ) , 'email');
	 		}

	 		if ( field.hasClass('regex') ) {
	 			var pattern = field.attr('regex');
	 			valid &= f.evaluate(field, !field.val().length || (  field.val().length > 0 && field.val().match(pattern) ) , 'regex');
	 		}

	 		if ( field.hasClass('match') ) {
	 			var matchfield = $('[name="' + field.attr('match') + '"]');
	 			console.log(matchfield)
	 			valid &= f.evaluate(field, !field.val().length || field.val() == matchfield.val() , 'match');
	 		}

	 		if ( field.hasClass('atleastone') ) {
	 			var relatives = $('input, select, textarea').filter('[rel="' + field.attr('rel') + '"]');
	 			var filledcount = 0;

	 			relatives.each( function(){
	 				var field = $(this);
			 		var type = f.getType(field);
					if( type == 'checkbox' || type == 'radio' ) { 
						filledcount += f.evaluate(field, filledcount > 0 || field.prop('checked'), 'atleastone') ? 1 : 0; 
					}
		 			else { 
		 				filledcount += f.evaluate(field, filledcount > 0 || (field.val().length > 0 && field.val() != ""), 'atleastone') ? 1 : 0; 
		 			}
	 			});

	 			valid &= (filledcount > 0);
	 		}

	 		if ( valid ) { 
	 			field.removeClass('invalid');
	 		} else { 
	 			field.addClass('invalid');
	 		}
	 	}
	 }

  	/* where the magic hapens */
    return this.each(function() {

    	var form = $(this);
    	
    	form.find("input, select, textarea").each( f.initializeField );

    	form.on('submit', f.validate );

    });  
 };
})(jQuery);  