<?
	/**
	* 
	*/
	class Site_Portfolio
	{
		
		/* Defaults to page editing */
		function index($arg){
			 $objPortfolio = new Package_Portfolio();
			 try {
			   if (count($arg) != 1) throw new Exception();
			   switch ($arg[0]){
			   	 case "sync":	
			   	 case "ifaLogout":
			   	 case "printPolicy":	
			   	   eval("\$objPortfolio->" . $arg[0] . "();");
			   	   break;
			   	 default: throw new Exception();
			   }	 
			 } catch (Exception $objE) {
			 	 if (count($arg) == 2 && $arg[0] == "printPolicy" && is_numeric($arg[1])){
			 	 	 $objPortfolio->printPolicy($arg[1]);
			 	 }else{	 
			     $objPortfolio->portfolio();
			   }
			 }  

			//DEBUG::show();
		}

		function logout(){
			Package::load('Login')->logout();
		}
	}