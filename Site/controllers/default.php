<?
/**
* 
*/
class Site_Default extends Controller
{
	/* this is the home page */	
	function index($arg)
	{
		if ( isset($arg) ){
			if (isset($arg[0]) && $arg[0] == "de"){
				$_SESSION["lang"] = 2;		
				array_shift($arg);
			} else {
				$_SESSION["lang"] = 1;							
			}
			Request::load('Package_Pages','show',$arg);
		}
		else{
			Request::load('Package_Pages','showDefault');	
		}

//		DEBUG::show();
	}

	function search($args){
		parse_str( trim($args[0],'?') );
		//echo "searchin for $for";
		Request::load('Package_Pages','search',$for);
	}
	
	function lang($arg){
		$_SESSION["lang"] = $arg[0] == "de" ? 2 : 1;		
		$strLocation = empty($_SERVER["HTTP_REFERER"]) ? "http://" . $_SERVER["HTTP_HOST"] : $_SERVER["HTTP_REFERER"];
		header("Location: $strLocation");		
	}
}
