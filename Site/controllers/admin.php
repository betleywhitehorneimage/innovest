<?
	/**
	* 
	*/
	class Site_Admin
	{
		
		/* Defaults to page editing */
		function index($arg)
		{
			
			if (isset($arg[1]) && $arg[1] == "de"){
				$_SESSION["lang"] = 2;		
				$arrTemp = array();
				for ($i=0; $i < count($arg); $i++) if ($i != 1) $arrTemp[] = $arg[$i];
				$arg = $arrTemp;
			} elseif (isset($arg[0]) && $arg[0] == "de"){
				$_SESSION["lang"] = 2;	
				$arg = array();
			} else {
				if (isset($arg[0]) && $arg[0] == "en") $arg = array();
				$_SESSION["lang"] = 1;							
			}
			
			// If login fails it will exit, if it is logged in it will pass
			if ( ! Package::load('Login')->isLoggedIn() ){
				Package::load('Login')->login();
			}
			
			$page = isset($arg[0]) ? $arg[0] : null;
			$action = isset($arg[1]) ? $arg[1] : 'get';

			switch ($action) {
				case 'update':
					Request::load('Package_Pages','update',$page,$_POST);
					$slug = DB::get_one("SELECT get_fullpath(?)",array($page));
					header("Location: " . SITE_ROOT . "admin/" . $page . '/' . $slug);
					break;
				case 'delete':
					Request::load('Package_Pages','delete',$page);
					header("Location: " . SITE_ROOT . "admin/");
					break;	
				case 'insert':
					$newid = Request::load('Package_Pages','insert',$_POST);
					$slug = DB::get_one("SELECT get_fullpath(?)",array($newid));					
					header("Location: " . SITE_ROOT . "admin/" . $newid . '/' . $slug);
					break;			
				default:
					Request::load('Package_Pages','editor',$page);
					break;
			}

			DEBUG::show();
		}

		function logout(){
			Package::load('Login')->logout();
		}
		
	}